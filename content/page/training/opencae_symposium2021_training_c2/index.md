---
title: オープンCAEシンポジウム2021トレーニングC2
author: imano
type: page
---
## C2 : OpenModelicaによる流体システムモデリング (初級者向け)
* 講師 : zeta_plusplus ( Modelica勉強会 )
* 講習概要 : OpenModelicaのFluidライブラリを用いた簡単な流体システムのモデリング＆解析を行います．今回はOpenModelicaでのモデリング基本手順から初級者向けに丁寧に解説し，使うコンポーネントもMSLのFluid（＋Thermal，Mechanical）に限定します．
* 受講にあたって必要な環境
	* 講習に必要なPCおよびソフトウエア環境を下記に示します．
	* マシンスペック
		* 下記ソフトウェアをインストール・動作させられれば特に問いません．
		* 比較的非力なポケットPC「GPD pocket」でも動作が確認されており，標準的なノートPCであれば問題ありません．
	* OS
		* Windows 10 64-bit
		* 下記ソフトウェアをインストール・動作させられれば他のOSでも構いませんが，OS違いによる不具合・トラブルは各自で対応願います．
	* ソフトウェア
		* OpenModelica 1.13または1.18を事前インストール・起動確認願います．
			* [OpenModelica 1.13][1]
			* [OpenModelica 1.18][2]
		* 上記リンク先の「OpenModelica-v1.13.0-64bit.exe」/「OpenModelica-v1.18.0-64bit.exe」をダウンロード・実行して下さい．
		* 他バージョンを利用されても構いませんが，バージョン違いによる不具合・トラブルは各自での対応をお願いします．
		* 他バージョンをインストール済みの方も，下記リンクに従って1.13または1.18をインストールすることをお薦めします．
			* [OpenModelica　バージョン違いインストール][3]
		* ver1.13 と1.18では，モデル構築作業中の設定手順が一部異なります．
		* 1.18の方がGUIからの操作で簡単に設定できるようになっています．講習は1.13，1.18両verでの手順を取り上げます．
	* 例題収録ライブラリ
		* [WalkingInWorldOfThermoFluid][4]
		* 必須では有りませんが，演習の答え合わせ用に手元にダウンロードしておくか，オンライン参照できる準備をしておくことを推奨します．
		* ライブラリは頻繁に更新しておりますので，ダウンロードの場合は講習数日前位にお願いします．
	* 加えて講習内容・資料ですが，下記リンク記事の例題（２つ）＋同レベルの例題１つか２つ程度をまとめたものにしようと考えております．今回はハンズオンレベル・入門者向けレベルのものを，その場で実演しながら受講者にもモデル作成して頂くつもりです．
		* [Modelicaによる熱流体の世界の歩き方・FlowResistance_ex01][5]
		* [Modelicaによる熱流体の世界の歩き方・PumpingSystem_ex01][6]

 [1]: https://build.openmodelica.org/omc/builds/windows/releases/1.13/final/64bit/
 [2]: https://build.openmodelica.org/omc/builds/windows/releases/1.18/0/64bit/
 [3]: https://qiita.com/UedaShigenori/items/052c51567ecbf3761807
 [4]: https://github.com/zeta-plusplus/WalkingInWorldOfThermoFluid
 [5]: https://virtualmbdlabmodelica.net/walkinginworldofthermofluid_flowresistance_ex01/
 [6]: https://virtualmbdlabmodelica.net/walkingthermofluid_pumpingsystem_ex01/
