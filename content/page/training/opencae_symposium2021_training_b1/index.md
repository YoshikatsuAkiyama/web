---
title: オープンCAEシンポジウム2021トレーニングB1
author: imano
type: page
---
## B1 : モデリングトレーニング基礎からのFreeCAD
* 講師 : 坪田 遼 ( XSim )
* 講習概要 : オープンソースの3次元CADであるFreeCADは業務用途，教育用途で必要なだけのマシンにインストールでき，自由な再配布，カスタマイズが可能です．FreeCADの利用を検討されている方，また社内利用の推進を検討されている方向けに操作方法を中心にFreeCADを解説します．
* 受講にあたって必要な環境
	* FreeCADバージョン0.19を使用します．
	* またOSに関してはWindowsを推奨します．
	* 公式のインストーラー配布サイトよりインストーラーをダウンロードし，インストールと起動確認を行ってください．
	* インストール方法に関しては以下のサイトを参照してください．
		* [XSim FreeCAD Windows でのインストール方法][1]
		* [XSim FreeCAD 起動しない場合に確認すること][2]

 [1]: https://www.xsim.info/articles/FreeCAD/HowTo/Install-on-Windows.html
 [2]: https://www.xsim.info/articles/FreeCAD/HowTo/If-FreeCAD-fail-to-start.html

